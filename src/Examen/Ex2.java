package Examen;

public class Ex2 {

    public static void main(String[] args) {
        MyThread t1 = new MyThread("StrThread1");
        MyThread t2 = new MyThread("StrThread2");

        t1.start();
        t2.start();
    }
}

class MyThread extends Thread{
    private String name;

    public MyThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 10; i++){
            System.out.println('['+ this.name + "] - [" + i + ']');
            try {
                Thread.sleep(4000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}